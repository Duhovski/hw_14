#include <iostream>
#include <string>

int main()
{
	std::string word;
	int letterCount;

	std::cout << "Type any word: ";
	std::cin >> word; 

	std::cout << word << "\n";

	letterCount = word.length();
	std::cout << "Letter in words: " << letterCount << "\n";
	
	std::cout << "First letter: " << word[0] << "\n"; //First letter
	std::cout << "Last letter: " << word[letterCount - 1] << "\n"; //Last letter

	return 0;
}